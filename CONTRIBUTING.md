# Enabled debug

Sometimes it's helpful to have some more information during
execution. That is where debug mode helps:

    cargo run --features=debug
